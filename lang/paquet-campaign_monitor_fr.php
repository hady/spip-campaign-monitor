<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'campaign_monitor_description' => 'Se connecter à Campaign Monitor pour y inscrire des abonnés à une newsletter à partir de SPIP',
	'campaign_monitor_nom' => 'API Campaign Monitor',
	'campaign_monitor_slogan' => '',
);