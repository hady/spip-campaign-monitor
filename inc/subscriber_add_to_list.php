<?php
/**
 * Fonctions utiles au plugin API Campaing Monitor
 *
 * @plugin     API Campaing Monitor
 * @copyright  2016
 * @author     Apsulis
 * @licence    GNU/GPL
 * @package    SPIP\Campaign_monitor\Fonctions
 */

if (!defined('_ECRIRE_INC_VERSION')) return;

/**
 * Inscrire un email sur une liste campaign monitor
 *
 * @link https://www.campaignmonitor.com/api/subscribers/#adding_a_subscriber
 * @link http://campaignmonitor.github.io/createsend-php/
 *
 * @param string $email Email à inscrire
 * @return boolean true/false si succes ou erreur technique
 *
 * @todo se brancher sur la config (BO) pour l'api_key et l'id_liste
**/
function campaign_monitor_add_to_list($email=''){
	if (!$email) return false;

	$cm_client_api_key = lire_config('campaign_monitor/cm_client_api_key');
	$cm_id_list        = lire_config('campaign_monitor/cm_id_list');

	if (!$cm_client_api_key || !$cm_id_list) return false;

	include_spip('lib/campaignmonitor-createsend-php/csrest_subscribers');
	include_spip('inc/cm_outils');

	$auth = array('api_key' => $cm_client_api_key); // Indiquer l'api_key du client

	$wrap = new CS_REST_Subscribers($cm_id_list, $auth); // Indiquer l'id de la liste
	$result = $wrap -> add(array(
		'EmailAddress' => $email,
		'Name' => '',
		'Resubscribe' => true
	));

	cm_log_add_to_list($email,$result->http_status_code);

	if($result->was_successful()) {
		return true;
	} else {
		return false;
	}
}