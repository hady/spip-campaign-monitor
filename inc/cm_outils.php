<?php
/**
 * Fonctions utiles au plugin API Campaing Monitor
 *
 * @plugin     API Campaing Monitor
 * @copyright  2016
 * @author     Apsulis
 * @licence    GNU/GPL
 * @package    SPIP\Campaign_monitor\Fonctions
 */

if (!defined('_ECRIRE_INC_VERSION')) return;

function cm_log_add_to_list($email,$statut){
	spip_log("Inscription de ".$email." avec le statut : ".$statut." (plus d'infos https://www.campaignmonitor.com/api/subscribers/#adding_a_subscriber) ", "campaign_monitor_add_to_list" . _LOG_INFO_IMPORTANTE);
}